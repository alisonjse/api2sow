<?php


namespace App\Services;

use App\Repositories\CidadaoRepository;
use Illuminate\Support\Facades\DB;

class CidadaoService
{
    private $cidadaoRepository;

    public function __construct(CidadaoRepository $cidadaoRepository)
    {
        $this->cidadaoRepository = $cidadaoRepository;
    }

    /**
     * Metodo responsavel por encapsular a logica de criação de um cidadao.
     *
     * @return object
     *
     */

    public function criarUmCidadao(string $nome, string $sobrenome, string $cpf, string $celular, string $email, string $cep)
    {
        $enderecoService = app(EnderecoService::class);
        $contatoService = app(ContatoService::class);
        $resposta = $enderecoService->buscarEnderecoPeloCep($cep);
        if($resposta->getData()->errors){
            return $resposta->throwResponse();
        }
        $endereco = json_decode($resposta->getData()->data);
        DB::beginTransaction();
        try {
            $novoCidadao = $this->cidadaoRepository->inserirCidadao($nome, $sobrenome, $cpf);
            if($novoCidadao){
                $novoContato = $contatoService->criarUmContatoParaCidadao($celular, $email, $novoCidadao->id);
                $novoEndereco = $enderecoService->criarUmEnderecoParaCidadao(str_replace('-','',$endereco->cep), $endereco->logradouro, $endereco->bairro, $endereco->localidade, $endereco->uf, $novoCidadao->id);
                if($novoContato && $novoEndereco){
                    DB::commit();
                    return response()->success($novoCidadao, 202);
                }
            }
            throw new \Exception("Erro ao inserir um novo cidadão");
        }catch (\Exception $e){
            DB::rollback();
            return response()->error($e->getMessage(), 400);
        }
    }

    /**
     * Metodo responsavel por encapsular a logica de atualização de um cidadao.
     *
     * @return object
     *
     */

    public function atualizarUmCidadao(string $nome, string $sobrenome, string $cpf, string $celular, string $email, string $cep, int $id)
    {
        $enderecoService = app(EnderecoService::class);
        $contatoService = app(ContatoService::class);
        $resposta = $enderecoService->buscarEnderecoPeloCep($cep);
        if($resposta->getData()->errors){
            return $resposta->throwResponse();
        }
        $endereco = json_decode($resposta->getData()->data);
        DB::beginTransaction();
        try {
            $cidadaoAtualizado = $this->cidadaoRepository->atualizarCidadao($id,$nome, $sobrenome, $cpf);
            if (empty($cidadaoAtualizado)) {
                throw new \Exception("Cidadão não encontrado na base de dados");
            }
            $contatoAtualizado = $contatoService->atualizarUmContatoParaCidadao($celular, $email, $cidadaoAtualizado->id);
            $enderecoAtualizado = $enderecoService->atualizarUmEnderecoParaCidadao(str_replace('-','',$endereco->cep), $endereco->logradouro, $endereco->bairro, $endereco->localidade, $endereco->uf, $cidadaoAtualizado->id);
            if($contatoAtualizado->getData()->errors == false && $enderecoAtualizado->getData()->errors == false) {
                DB::commit();
                return response()->success($cidadaoAtualizado, 202);
            }
            throw new \Exception("Erro ao atualizar cidadão");
        }catch (\Exception $e){
            DB::rollback();
            return response()->error($e->getMessage(), 400);
        }
    }
}
